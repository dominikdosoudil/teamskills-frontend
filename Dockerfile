FROM alpine

RUN apk update
RUN apk add python3 curl

# install elm compiler
RUN curl -L -o elm.gz https://github.com/elm/compiler/releases/download/0.19.1/binary-for-linux-64-bit.gz
RUN gunzip elm.gz
RUN chmod +x elm
RUN mv elm /usr/local/bin/

COPY . /opt/s-frontend/
WORKDIR /opt/s-frontend
RUN elm make src/Main.elm
CMD python3 -m http.server 80
