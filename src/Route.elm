module Route exposing (..)

import Url exposing (Url)
import Url.Parser exposing (..)


type CRUD
    = Get Int
    | List
    | Add


type Route
    = Home
    | NotFound404
    | Persons
    | PersonAdd
    | Person Int
    | Skill CRUD
    | Project CRUD


parser : Parser (Route -> a) a
parser =
    oneOf
        [ map Home top

        -- person
        , map PersonAdd (s "persons" </> s "add")
        , map Person (s "persons" </> int)
        , map Persons (s "persons")

        -- skill
        , map (\id -> Skill (Get id)) (s "skills" </> int)
        , map (Skill List) (s "skills")
        , map (Skill Add) (s "skills" </> s "add")

        -- project
        , map (\id -> Project (Get id)) (s "projects" </> int)
        , map (Project List) (s "projects")
        , map (Project Add) (s "projects" </> s "add")
        ]


fromUrl : Url -> Route
fromUrl url =
    Maybe.withDefault NotFound404 (parse parser { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing })
