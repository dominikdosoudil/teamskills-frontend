module Page.Skill exposing (..)

import Browser.Navigation as Nav
import Constants exposing (apiUrl, crossCode, magnifyingGlassCode)
import Element exposing (Element, column, el, row, shrink, spacing, table, text)
import Element.Input as Input
import Framework.Button exposing (button, buttonLink)
import Framework.FormField exposing (inputText)
import Framework.Modifier exposing (Modifier(..))
import Framework.Typography exposing (h2)
import Http
import Json.Decode exposing (Decoder, field, int, list, map2, string)
import Json.Encode as Encode
import Route exposing (CRUD(..))
import Utils exposing (codeToString, httpDelete, httpPut, viewLoading)



-- INIT


type EditMode
    = Enable
    | Disable
    | Save


type FormField
    = Name


type alias Form =
    { name : String }


initForm : Maybe Skill -> Form
initForm maybeSkill =
    case maybeSkill of
        Nothing ->
            { name = "" }

        Just skill ->
            { name = skill.name }


type alias Model =
    { skills : List Skill
    , skill : Maybe Skill
    , form : Form
    , editing : Bool
    }


init =
    Model
        []
        Nothing
        (initForm Nothing)
        False



-- UPDATE


type Msg
    = NoOp
    | GotSkills (Result Http.Error (List Skill))
    | GotSkill (Result Http.Error Skill)
    | DeleteRequest Int
    | SkillDeleted Int (Result Http.Error ())
    | SkillCreated (Result Http.Error Skill)
    | FormChange FormField String
    | FormSubmit
    | Edit EditMode


updateForm : FormField -> String -> Form -> Form
updateForm field value form =
    case field of
        Name ->
            { form | name = value }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotSkills result ->
            case result of
                Ok skills ->
                    ( { model | skills = skills }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        GotSkill result ->
            ( { model
                | skill =
                    case result of
                        Ok skill ->
                            Just skill

                        Err _ ->
                            Nothing
              }
            , Cmd.none
            )

        DeleteRequest id ->
            ( model, deleteSkill id )

        SkillDeleted id result ->
            case result of
                Ok () ->
                    ( model, Nav.load "#/skills" )

                Err error ->
                    ( model, Cmd.none )

        SkillCreated result ->
            case result of
                Ok _ ->
                    ( { model | form = initForm Nothing }, Nav.load "#/skills" )

                Err _ ->
                    ( model, Cmd.none )

        FormChange formField value ->
            case formField of
                Name ->
                    ( { model | form = updateForm formField value model.form }, Cmd.none )

        FormSubmit ->
            ( model, createSkill model.form )

        Edit Enable ->
            ( { model | editing = True, form = initForm model.skill }, Cmd.none )

        Edit Disable ->
            ( { model | editing = False, form = initForm Nothing }, Cmd.none )

        Edit Save ->
            case model.skill of
                Just skill ->
                    ( { model | editing = False, form = initForm Nothing }, editSkill skill.id model.form GotSkill )

                Nothing ->
                    ( model, Cmd.none )



-- VIEW


view : CRUD -> Model -> Element Msg
view crud model =
    case crud of
        List ->
            column [ spacing 10 ]
                [ h2 [] (text "Skills")
                , table [ spacing 10 ]
                    { data = model.skills
                    , columns =
                        [ { header = text "id"
                          , width = shrink
                          , view = \s -> text (String.fromInt s.id)
                          }
                        , { header = text "name"
                          , width = shrink
                          , view = \s -> text s.name
                          }
                        , { header = text "detail"
                          , width = shrink
                          , view = \s -> buttonLink [] ("#/skills/" ++ String.fromInt s.id) (codeToString magnifyingGlassCode)
                          }
                        , { header = text "delete"
                          , width = shrink
                          , view = \s -> button [] (Just (DeleteRequest s.id)) (codeToString crossCode)
                          }
                        ]
                    }
                , buttonLink [ Medium, Success, Outlined ] "#/skills/add" "+"
                ]

        Get id ->
            column [ spacing 20 ]
                (case model.skill of
                    Just skill ->
                        case model.editing of
                            False ->
                                [ row [ spacing 20 ] [ h2 [] (text "Skill"), button [] (Just (Edit Enable)) "Edit" ]
                                , text (String.fromInt skill.id)
                                , text skill.name
                                ]

                            True ->
                                [ viewForm model.form
                                , button [] (Just (Edit Save)) "Save"
                                ]

                    Nothing ->
                        [ viewLoading ]
                )

        Add ->
            column [ spacing 20 ]
                [ h2 [] (text "Add skill")
                , viewForm model.form
                , button [ Medium, Outlined, Success ] (Just FormSubmit) "Submit"
                ]


viewForm : Form -> Element Msg
viewForm form =
    inputText []
        { field = Name
        , label = text "Name"
        , fieldValue = form.name
        , inputType = Input.text
        , maybeFieldFocused = Nothing
        , inputTypeAttrs = []
        , msgOnChange = FormChange
        , msgOnFocus = \_ -> NoOp
        , msgOnLoseFocus = \_ -> NoOp
        , maybeMsgOnEnter = Nothing
        , helperText = Nothing
        }



-- HTTP


fetchByRoute : CRUD -> Cmd Msg
fetchByRoute crud =
    case crud of
        Get id ->
            getSkill id GotSkill

        List ->
            getSkills GotSkills

        Add ->
            Cmd.map (always NoOp) Cmd.none


getSkills : (Result Http.Error (List Skill) -> msg) -> Cmd msg
getSkills msg =
    Http.get
        { url = apiUrl ++ "skills"
        , expect = Http.expectJson msg (list skillDecoder)
        }


getSkill : Int -> (Result Http.Error Skill -> msg) -> Cmd msg
getSkill id msg =
    Http.get
        { url = apiUrl ++ "skills/" ++ String.fromInt id
        , expect = Http.expectJson msg skillDecoder
        }


createSkill : Form -> Cmd Msg
createSkill form =
    Http.post
        { url = apiUrl ++ "skills"
        , body = Http.jsonBody (Encode.string form.name)
        , expect = Http.expectJson SkillCreated skillDecoder
        }


deleteSkill : Int -> Cmd Msg
deleteSkill id =
    httpDelete
        { url = apiUrl ++ "skills/" ++ String.fromInt id
        , expect = Http.expectWhatever (SkillDeleted id)
        }


editSkill : Int -> Form -> (Result Http.Error Skill -> msg) -> Cmd msg
editSkill id form msg =
    httpPut
        { url = apiUrl ++ "skills/" ++ String.fromInt id
        , body = Http.jsonBody (Encode.string form.name)
        , expect = Http.expectJson msg skillDecoder
        }



-- JSON


type alias Skill =
    { id : Int
    , name : String
    }


skillDecoder : Decoder Skill
skillDecoder =
    map2 Skill
        (field "id" int)
        (field "name" string)
