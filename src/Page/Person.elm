module Page.Person exposing (..)

-- INIT

import Browser.Navigation as Nav
import Constants exposing (apiUrl, crossCode, magnifyingGlassCode)
import Element exposing (Element, column, el, padding, row, shrink, spacing, width)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Framework.Button as Button
import Framework.Color exposing (green, yellow)
import Framework.FormField exposing (inputText)
import Framework.Modifier as Modifier exposing (Modifier(..))
import Framework.Typography as Typography
import Http exposing (Response)
import Json.Decode exposing (Decoder, field, int, list, map3, string)
import Json.Encode as Encode
import Page.Skill exposing (Skill, getSkills, skillDecoder)
import Utils exposing (codeToString, httpDelete, httpPut, viewLoading)


type alias Form =
    { name : String }


type FormState
    = Init
    | Submitting


type alias Model =
    { persons : List Person
    , person : Maybe Person
    , form : Form
    , formState : FormState
    , skillAddUnlocked : Bool
    , availableSkills : List Skill
    , editing : Bool
    }


addPersonSkills : Model -> List Skill -> Model
addPersonSkills model skills =
    case model.person of
        Just p ->
            { model | person = Just { p | skillIds = Loaded skills } }

        Nothing ->
            model


initForm : Maybe Person -> Form
initForm maybePerson =
    case maybePerson of
        Nothing ->
            { name = "" }

        Just person ->
            { name = person.name }


init =
    Model
        []
        Nothing
        (initForm Nothing)
        Init
        False
        []
        False



-- UPDATE


type FormField
    = Name


type EditMode
    = Enable
    | Disable
    | Save


type Msg
    = NoOp
    | FormChange FormField String
    | FormSubmit
    | PersonCreated (Result Http.Error Person)
    | DeleteRequest Int
    | PersonDeleted Int (Result Http.Error ())
    | GotPersons (Result Http.Error (List Person))
    | GotPerson (Result Http.Error Person)
    | GotPersonSkills (Result Http.Error (List Skill))
    | ClearPerson
    | RemoveSkillRequest Int
    | SkillRemoved Int Int (Result Http.Error ())
    | AddSkill
    | GotAvailableSkills (Result Http.Error (List Skill))
    | AddSkillRequest Int
    | Edit EditMode


updateForm : FormField -> String -> Form -> Form
updateForm field value model =
    case field of
        Name ->
            { model | name = value }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        FormChange field value ->
            ( { model | form = updateForm field value model.form }, Cmd.none )

        FormSubmit ->
            ( { model | formState = Submitting }, createPerson model.form )

        PersonCreated postPersonResult ->
            case postPersonResult of
                Ok person ->
                    ( { model | formState = Init, form = init.form }, Nav.load "#/persons" )

                Err error ->
                    Debug.log "error" ( model, Cmd.none )

        DeleteRequest id ->
            ( model, deletePerson id )

        PersonDeleted personId deletePersonResult ->
            case deletePersonResult of
                Ok str ->
                    ( model, Nav.load "#/persons" )

                Err error ->
                    Debug.log "error" ( model, Cmd.none )

        GotPersons getPersonsResult ->
            case getPersonsResult of
                Ok persons ->
                    Debug.log "persons"
                        ( { model | persons = persons }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        GotPerson getPersonResult ->
            case getPersonResult of
                Ok person ->
                    ( { model | person = Just person, skillAddUnlocked = False }
                    , case person.skillIds of
                        Loading skillIds ->
                            getPersonSkills skillIds

                        _ ->
                            Cmd.none
                    )

                Err error ->
                    ( model, Cmd.none )

        GotPersonSkills getPersonSkillsResult ->
            case getPersonSkillsResult of
                Ok personSkills ->
                    ( addPersonSkills model personSkills, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        ClearPerson ->
            ( { model | person = Nothing }, Cmd.none )

        RemoveSkillRequest skillId ->
            let
                i =
                    Debug.log "hmmm O.o" model.person
            in
            case model.person of
                Just p ->
                    ( model, removeSkill skillId p.id )

                Nothing ->
                    ( model, Cmd.none )

        SkillRemoved skillId personId result ->
            case result of
                Ok () ->
                    ( model, Nav.load ("#/persons/" ++ String.fromInt personId) )

                Err err ->
                    ( model, Cmd.none )

        AddSkill ->
            ( { model | skillAddUnlocked = True }, getSkills GotAvailableSkills )

        GotAvailableSkills result ->
            case result of
                Ok skills ->
                    ( { model | availableSkills = skills }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        AddSkillRequest skillId ->
            case model.person of
                Just person ->
                    ( { model | skillAddUnlocked = False }, addSkill skillId person.id )

                Nothing ->
                    ( { model | skillAddUnlocked = False }, Cmd.none )

        Edit Enable ->
            ( { model | editing = True, form = initForm model.person }, Cmd.none )

        Edit Disable ->
            ( { model | editing = False, form = initForm Nothing }, Cmd.none )

        Edit Save ->
            case model.person of
                Just person ->
                    ( { model | editing = False, form = initForm Nothing }, editPerson person.id model.form )

                Nothing ->
                    ( model, Cmd.none )



-- VIEW


viewPersons : List Person -> Element Msg
viewPersons persons =
    column [ spacing 10 ]
        [ Typography.h2 [] (Element.text "Persons")
        , Element.table [ spacing 10 ]
            { data = persons
            , columns =
                [ { header = Element.text "id"
                  , width = shrink
                  , view = \p -> Element.text (String.fromInt p.id)
                  }
                , { header = Element.text "name"
                  , width = shrink
                  , view = \p -> Element.text p.name
                  }
                , { header = Element.text "detail"
                  , width = shrink
                  , view = \p -> Button.buttonLink [] ("#/persons/" ++ String.fromInt p.id) (codeToString magnifyingGlassCode)
                  }
                , { header = Element.text "delete"
                  , width = shrink
                  , view = \p -> Button.button [] (Just (DeleteRequest p.id)) (codeToString crossCode)
                  }
                ]
            }
        , Button.buttonLink [ Modifier.Medium, Success, Outlined ] "#/persons/add" "+"
        ]


viewSkill : Skill -> Element Msg
viewSkill skill =
    el
        [ Background.color yellow
        , Border.rounded 3
        , padding 7
        ]
        (row
            [ spacing 10 ]
            [ Element.text skill.name, Button.button [] (Just (RemoveSkillRequest skill.id)) (codeToString crossCode) ]
        )


viewAvailableSkill : Skill -> Element Msg
viewAvailableSkill skill =
    el
        [ Background.color green
        , Border.rounded 3
        , padding 4
        ]
        (Button.button [] (Just (AddSkillRequest skill.id)) skill.name)


viewPerson : Model -> Element Msg
viewPerson model =
    case model.person of
        Nothing ->
            viewLoading

        Just person ->
            case model.editing of
                False ->
                    column [ spacing 20, width shrink ]
                        [ row [ spacing 20 ] [ Typography.h2 [] (Element.text "Person"), Button.button [] (Just (Edit Enable)) "Edit" ]
                        , el [] (Element.text (String.fromInt person.id))
                        , el [] (Element.text person.name)
                        , row [ spacing 20 ]
                            [ case person.skillIds of
                                Loaded skills ->
                                    row [ spacing 10 ] (List.map viewSkill skills)

                                _ ->
                                    viewLoading
                            , case model.skillAddUnlocked of
                                True ->
                                    let
                                        personSkillIds =
                                            case person.skillIds of
                                                Loaded skills ->
                                                    List.map .id skills

                                                Loading skillIds ->
                                                    skillIds
                                    in
                                    row [] (List.map viewAvailableSkill (List.filter (\s -> not (List.member s.id personSkillIds)) model.availableSkills))

                                False ->
                                    Button.button [ Modifier.Medium, Success, Outlined ] (Just AddSkill) "+"
                            ]
                        ]

                True ->
                    column [ spacing 20 ]
                        [ Button.button [] (Just (Edit Save)) "Save"
                        , viewPersonForm model.form
                        ]


viewPersonForm : Form -> Element Msg
viewPersonForm form =
    inputText [ spacing 20 ]
        { field = Name
        , label = row [ spacing 20 ] [ el [] <| Element.text "Name" ]
        , fieldValue = form.name
        , inputType = Input.text
        , maybeFieldFocused = Nothing
        , inputTypeAttrs = []
        , msgOnChange = FormChange
        , msgOnFocus = \_ -> NoOp
        , msgOnLoseFocus = \_ -> NoOp
        , maybeMsgOnEnter = Nothing
        , helperText = Nothing
        }


viewPersonAdd : Model -> Element Msg
viewPersonAdd model =
    column [ spacing 20 ]
        [ Typography.h2 [] (Element.text "Add person")
        , viewPersonForm model.form
        , Button.button [ Modifier.Medium, Outlined, Success ] (Just FormSubmit) "Submit"
        ]



-- HTTP


editPerson : Int -> Form -> Cmd Msg
editPerson id form =
    httpPut
        { url = apiUrl ++ "persons/" ++ String.fromInt id
        , body = Http.jsonBody (Encode.string form.name)
        , expect = Http.expectJson GotPerson personDecoder
        }


createPerson : Form -> Cmd Msg
createPerson form =
    Http.post
        { url = apiUrl ++ "persons"
        , body = Http.jsonBody (personEncoder form)
        , expect = Http.expectJson PersonCreated personDecoder
        }


deletePerson : Int -> Cmd Msg
deletePerson id =
    httpDelete
        { url = apiUrl ++ "persons/" ++ String.fromInt id
        , expect = Http.expectWhatever (PersonDeleted id)
        }


getPersonSkills : List Int -> Cmd Msg
getPersonSkills skillIds =
    Http.get
        { url = apiUrl ++ "skills/?ids=" ++ String.join "," (List.map String.fromInt skillIds)
        , expect = Http.expectJson GotPersonSkills (list skillDecoder)
        }


getPersons : (Result Http.Error (List Person) -> msg) -> Cmd msg
getPersons msg =
    Http.get
        { url = apiUrl ++ "persons"
        , expect = Http.expectJson msg (list personDecoder)
        }


getPerson : Int -> Cmd Msg
getPerson personId =
    Http.get
        { url = apiUrl ++ "persons/" ++ String.fromInt personId
        , expect = Http.expectJson GotPerson personDecoder
        }


{-| Adds skill to person
-}
addSkill : Int -> Int -> Cmd Msg
addSkill skillId personId =
    Http.post
        { url = apiUrl ++ "persons/" ++ String.fromInt personId ++ "/skills"
        , body = Http.jsonBody (Encode.int skillId)
        , expect = Http.expectJson GotPerson personDecoder
        }


{-| Removes skill from person
-}
removeSkill : Int -> Int -> Cmd Msg
removeSkill skillId personId =
    httpDelete
        { url = apiUrl ++ "persons/" ++ String.fromInt personId ++ "/skills/" ++ String.fromInt skillId
        , expect = Http.expectWhatever (SkillRemoved skillId personId)
        }



-- JSON


type Skills
    = Loading (List Int)
    | Loaded (List Skill)


type alias Person =
    { id : Int
    , name : String
    , skillIds : Skills
    }


personEncoder : Form -> Encode.Value
personEncoder form =
    Encode.object
        [ ( "name", Encode.string form.name )
        , ( "skillIds", Encode.list Encode.int [] )
        ]


personDecoder : Decoder Person
personDecoder =
    map3 Person
        (field "id" int)
        (field "name" string)
        (field "skillIds" (Json.Decode.map Loading (list int)))
