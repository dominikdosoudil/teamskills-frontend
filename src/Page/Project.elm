module Page.Project exposing (..)

import Browser.Navigation as Nav
import Constants exposing (apiUrl, crossCode, magnifyingGlassCode)
import Element exposing (Element, column, row, shrink, spacing, table, text)
import Element.Input as Input
import Framework.Button exposing (button, buttonLink)
import Framework.FormField exposing (inputText)
import Framework.Modifier exposing (Modifier(..))
import Framework.Typography exposing (h2)
import Http
import Input.MultiSelect exposing (Option, multiSelect)
import Json.Decode exposing (Decoder, field, int, list, map4, maybe, string)
import Json.Encode as Encode
import Page.Person exposing (Person, getPersons)
import Route exposing (CRUD(..))
import Utils exposing (codeToString, httpDelete, httpPut, viewLoading)



-- INIT


type EditMode
    = Enable
    | Disable
    | Save


type alias Form =
    { name : String
    , manDayEstimate : Maybe Int
    , personIds : List Int
    }


initForm : Maybe Project -> Form
initForm maybeProject =
    case maybeProject of
        Nothing ->
            Form "" Nothing []

        Just project ->
            Form project.name project.manDayEstimate project.personIds


type FormField
    = Name
    | ManDayEstimate
    | PersonIds Int


type alias Model =
    { projects : List Project
    , project : Maybe Project
    , editing : Bool
    , form : Form
    , availablePersons : List Person
    }


init : Model
init =
    Model
        []
        Nothing
        False
        (initForm Nothing)
        []



-- UPDATE


type InputType
    = Text FormField String
    | MultiSelect FormField


type Msg
    = NoOp
    | DeleteRequest Int
    | GotProject (Result Http.Error Project)
    | GotProjects (Result Http.Error (List Project))
    | ProjectDeleted Int (Result Http.Error ())
    | FormChange InputType
    | FormSubmit
    | ProjectCreated (Result Http.Error Project)
    | Edit EditMode
    | GotAvailablePersons (Result Http.Error (List Person))


updateForm : InputType -> Form -> Form
updateForm change form =
    case change of
        Text Name value ->
            { form | name = value }

        Text ManDayEstimate value ->
            case value of
                "" ->
                    { form | manDayEstimate = Nothing }

                _ ->
                    case String.toInt value of
                        Nothing ->
                            form

                        Just manDayEstimate ->
                            { form | manDayEstimate = Just manDayEstimate }

        Text _ _ ->
            form

        MultiSelect (PersonIds id) ->
            if List.member id form.personIds then
                { form | personIds = List.filter (\x -> x /= id) form.personIds }

            else
                { form | personIds = id :: form.personIds }

        MultiSelect _ ->
            form


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotProjects result ->
            case result of
                Ok projects ->
                    ( { model | projects = projects }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        GotProject result ->
            case result of
                Ok project ->
                    ( { model | project = Just project }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        DeleteRequest id ->
            ( model, deleteProject id (ProjectDeleted id) )

        ProjectDeleted id result ->
            case result of
                Ok () ->
                    ( model, Nav.load "#/projects" )

                Err error ->
                    ( model, Cmd.none )

        FormChange change ->
            ( { model | form = updateForm change model.form }, Cmd.none )

        FormSubmit ->
            ( { model | form = initForm Nothing }, createProject model.form ProjectCreated )

        ProjectCreated project ->
            ( model, Nav.load "#/projects" )

        Edit Enable ->
            ( { model | editing = True, form = initForm model.project }, getPersons GotAvailablePersons )

        Edit Disable ->
            ( { model | editing = False, form = initForm Nothing }, Cmd.none )

        Edit Save ->
            case model.project of
                Just project ->
                    ( { model | editing = False, form = initForm Nothing }, editProject project.id model.form GotProject )

                Nothing ->
                    ( model, Cmd.none )

        GotAvailablePersons resutlt ->
            case resutlt of
                Ok persons ->
                    ( { model | availablePersons = persons }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )



-- VIEW


viewForm : Form -> List Person -> Element Msg
viewForm form availablePersons =
    column [ spacing 20 ]
        [ inputText []
            { field = Name
            , label = text "Name"
            , fieldValue = form.name
            , inputType = Input.text
            , maybeFieldFocused = Nothing
            , inputTypeAttrs = []
            , msgOnChange = \field value -> FormChange (Text field value)
            , msgOnFocus = \_ -> NoOp
            , msgOnLoseFocus = \_ -> NoOp
            , maybeMsgOnEnter = Nothing
            , helperText = Nothing
            }
        , inputText []
            { field = ManDayEstimate
            , label = text "Estimate man days"
            , fieldValue =
                case form.manDayEstimate of
                    Just n ->
                        String.fromInt n

                    Nothing ->
                        ""
            , inputType = Input.text
            , maybeFieldFocused = Nothing
            , inputTypeAttrs = []
            , msgOnChange = \field value -> FormChange (Text field value)
            , msgOnFocus = \_ -> NoOp
            , msgOnLoseFocus = \_ -> NoOp
            , maybeMsgOnEnter = Nothing
            , helperText = Nothing
            }
        , multiSelect
            { options = List.map (\p -> Option (text p.name) p.id) availablePersons
            , values = form.personIds
            , handleClick = \value -> FormChange (MultiSelect (PersonIds value))
            }
        ]


view : CRUD -> Model -> Element Msg
view crud model =
    case crud of
        List ->
            column [ spacing 10 ]
                [ h2 [] (text "Projects")
                , table [ spacing 10 ]
                    { data = model.projects
                    , columns =
                        [ { header = text "id"
                          , width = shrink
                          , view = \s -> text (String.fromInt s.id)
                          }
                        , { header = text "name"
                          , width = shrink
                          , view = \s -> text s.name
                          }
                        , { header = text "manDayEstimate"
                          , width = shrink
                          , view =
                                \s ->
                                    text
                                        (case s.manDayEstimate of
                                            Nothing ->
                                                "/"

                                            Just i ->
                                                String.fromInt i
                                        )
                          }
                        , { header = text "detail"
                          , width = shrink
                          , view = \s -> buttonLink [] ("#/projects/" ++ String.fromInt s.id) (codeToString magnifyingGlassCode)
                          }
                        , { header = text "delete"
                          , width = shrink
                          , view = \s -> button [] (Just (DeleteRequest s.id)) (codeToString crossCode)
                          }
                        ]
                    }
                , buttonLink [ Medium, Success, Outlined ] "#/projects/add" "+"
                ]

        Get id ->
            case model.project of
                Nothing ->
                    viewLoading

                Just project ->
                    case model.editing of
                        False ->
                            column [ spacing 20 ]
                                [ row [ spacing 20 ] [ h2 [] (text "Project"), button [] (Just (Edit Enable)) "Edit" ]
                                , text project.name
                                , text
                                    (case project.manDayEstimate of
                                        Nothing ->
                                            "/"

                                        Just n ->
                                            String.fromInt n
                                    )
                                , row [ spacing 20 ]
                                    (List.map
                                        (\pId ->
                                            text
                                                (case List.head (List.filter (\availablePerson -> availablePerson.id == pId) model.availablePersons) of
                                                    Nothing ->
                                                        "loading..."

                                                    Just person ->
                                                        person.name
                                                )
                                        )
                                        project.personIds
                                    )
                                ]

                        True ->
                            column [ spacing 20 ]
                                [ row [ spacing 20 ] [ h2 [] (text "Edit project"), button [] (Just (Edit Disable)) "Cancel" ]
                                , viewForm model.form model.availablePersons
                                , button [] (Just (Edit Save)) "Save"
                                ]

        Add ->
            column [ spacing 20 ]
                [ h2 [] (text "Add project")
                , viewForm model.form model.availablePersons
                , button [ Medium, Outlined, Success ] (Just FormSubmit) "Submit"
                ]



-- HTTP


fetchByRoute : CRUD -> Cmd Msg
fetchByRoute crud =
    case crud of
        Get int ->
            Cmd.batch [ getProject int GotProject, getPersons GotAvailablePersons ]

        List ->
            getProjects GotProjects

        Add ->
            getPersons GotAvailablePersons


getProjects : (Result Http.Error (List Project) -> msg) -> Cmd msg
getProjects msg =
    Http.get
        { url = apiUrl ++ "projects"
        , expect = Http.expectJson msg (list projectDecoder)
        }


getProject : Int -> (Result Http.Error Project -> msg) -> Cmd msg
getProject id msg =
    Http.get
        { url = apiUrl ++ "projects/" ++ String.fromInt id
        , expect = Http.expectJson msg projectDecoder
        }


createProject : Form -> (Result Http.Error Project -> msg) -> Cmd msg
createProject form msg =
    Http.post
        { url = apiUrl ++ "projects"
        , body = Http.jsonBody (projectEncoder form)
        , expect = Http.expectJson msg projectDecoder
        }


editProject : Int -> Form -> (Result Http.Error Project -> msg) -> Cmd msg
editProject id form msg =
    httpPut
        { url = apiUrl ++ "projects/" ++ String.fromInt id
        , body = Http.jsonBody (projectEncoder form)
        , expect = Http.expectJson msg projectDecoder
        }


deleteProject : Int -> (Result Http.Error () -> msg) -> Cmd msg
deleteProject id msg =
    httpDelete
        { url = apiUrl ++ "projects/" ++ String.fromInt id
        , expect = Http.expectWhatever msg
        }



-- JSON


type alias Project =
    { id : Int
    , name : String
    , manDayEstimate : Maybe Int
    , personIds : List Int
    }


projectDecoder : Decoder Project
projectDecoder =
    map4 Project
        (field "id" int)
        (field "name" string)
        (field "manDayEstimate" (maybe int))
        (field "personIds" (list int))


projectEncoder : Form -> Encode.Value
projectEncoder form =
    Encode.object
        [ ( "name", Encode.string form.name )
        , ( "manDayEstimate"
          , case form.manDayEstimate of
                Nothing ->
                    Encode.null

                Just manDayEstimate ->
                    Encode.int manDayEstimate
          )
        , ( "personIds", Encode.list Encode.int form.personIds )
        ]
