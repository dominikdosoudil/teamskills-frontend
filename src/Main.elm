module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Element exposing (Element, column, layout, padding, spacing)
import Framework.Button as Button exposing (buttonLink)
import Framework.Modifier as Modifier exposing (..)
import Framework.Typography as Typography
import Html exposing (..)
import Http
import Page.Person exposing (Msg(..), Person, Skills(..), getPerson, getPersons, viewPerson, viewPersonAdd, viewPersons)
import Page.Project
import Page.Skill
import Route exposing (Route, fromUrl)
import Url exposing (Protocol(..))


type alias Document msg =
    { title : String
    , body : List (Html msg)
    }


{-| js interop, flags that are passed to elm app from javascript
-}
type alias Flags =
    ()


{-| following type is required so elm knows what flags will be passed to elm program
elm performs type validation (js to elm) of flags when instantiating the application
<https://guide.elm-lang.org/interop/flags.html>
-}
main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- INIT


{-| type of application state -
-}
type alias Model =
    { route : Route
    , key : Nav.Key
    , count : Int
    , personModel : Page.Person.Model
    , skillModel : Page.Skill.Model
    , projectModel : Page.Project.Model
    , httpError : Maybe Http.Error
    }


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    fromUrl url
        |> (\route ->
                ( Model
                    route
                    key
                    0
                    Page.Person.init
                    Page.Skill.init
                    Page.Project.init
                    Nothing
                , fetchByRoute route
                )
           )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- UPDATE


{-| type of messages that can be fired in the application
-}
type Msg
    = NoOp
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | Refresh
    | PersonMsg Page.Person.Msg
    | SkillMsg Page.Skill.Msg
    | ProjectMsg Page.Project.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            fromUrl url |> (\route -> ( { model | route = route }, fetchByRoute route ))

        Refresh ->
            ( model, fetchByRoute model.route )

        PersonMsg personMsg ->
            let
                ( personModel, cmd ) =
                    Page.Person.update personMsg model.personModel
            in
            ( { model | personModel = personModel }, Cmd.map PersonMsg cmd )

        SkillMsg skillMsg ->
            let
                ( skillModel, cmd ) =
                    Page.Skill.update skillMsg model.skillModel
            in
            ( { model | skillModel = skillModel }, Cmd.map SkillMsg cmd )

        ProjectMsg projectMsg ->
            let
                ( projectModel, cmd ) =
                    Page.Project.update projectMsg model.projectModel
            in
            ( { model | projectModel = projectModel }, Cmd.map ProjectMsg cmd )



-- VIEW


view : Model -> Document Msg
view model =
    Document "Team skills"
        [ div []
            [ layout
                []
                (column [ padding 20, spacing 10 ]
                    [ Typography.h1 [] (Element.text "Team skills")
                    , Button.button [ Modifier.Medium, Success, Outlined ] (Just Refresh) "Refresh"
                    , viewNav
                    , viewRouter model
                    ]
                )
            ]
        ]


viewNav =
    Element.row
        [ spacing 100 ]
        [ buttonLink [ Medium ] "#/persons" "Persons"
        , buttonLink [ Medium ] "#/skills" "Skills"
        , buttonLink [ Medium ] "#/projects" "Projects"
        ]


view404 =
    Element.el [] (Typography.h2 [] (Element.text "404 not found"))


viewRouter : Model -> Element Msg
viewRouter model =
    case model.route of
        Route.Persons ->
            Element.map PersonMsg (viewPersons model.personModel.persons)

        Route.Person personId ->
            Element.map PersonMsg (viewPerson model.personModel)

        Route.PersonAdd ->
            Element.map PersonMsg (viewPersonAdd model.personModel)

        Route.Skill crud ->
            Element.map SkillMsg (Page.Skill.view crud model.skillModel)

        Route.Project crud ->
            Element.map ProjectMsg (Page.Project.view crud model.projectModel)

        _ ->
            view404



-- HTTP


fetchByRoute : Route -> Cmd Msg
fetchByRoute route =
    case route of
        Route.Persons ->
            Cmd.map PersonMsg (getPersons GotPersons)

        Route.Person personId ->
            Cmd.batch [ Cmd.map PersonMsg (getPerson personId), Cmd.map (always PersonMsg ClearPerson) Cmd.none ]

        Route.Skill crud ->
            Cmd.map SkillMsg (Page.Skill.fetchByRoute crud)

        Route.Project crud ->
            Cmd.map ProjectMsg (Page.Project.fetchByRoute crud)

        _ ->
            Cmd.none
