module Utils exposing (..)

import Framework.Color as Color
import Framework.Spinner exposing (Spinner(..), spinner)
import Http exposing (Body, Expect)



-- VIEW


codeToString : Int -> String
codeToString code =
    String.fromChar (Char.fromCode code)


viewLoading =
    spinner Rotation 32 Color.black



-- HTTP


{-| syntax sugar for basic http DELETE requests
-}
httpDelete :
    { url : String
    , expect : Expect msg
    }
    -> Cmd msg
httpDelete { url, expect } =
    Http.request
        { method = "DELETE"
        , headers = [ Http.header "Accept" "*/*" ]
        , url = url
        , body = Http.emptyBody
        , expect = expect
        , timeout = Nothing
        , tracker = Nothing
        }


{-| syntax sugar for http PUT requests
-}
httpPut :
    { url : String
    , body : Body
    , expect : Expect msg
    }
    -> Cmd msg
httpPut { url, body, expect } =
    Http.request
        { method = "PUT"
        , headers = [ Http.header "Accept" "*/*" ]
        , url = url
        , body = body
        , expect = expect
        , timeout = Nothing
        , tracker = Nothing
        }
